Napisz metodę, która przyjmuje datę i czas jako parametr typu String w formacie `dd-MM-yyyy HH:mm`. 

Metoda następnie oblicza milisekundy, sekundy, minuty, godziny, dni, tygodnie, miesiące i lata, które minęły od podanej daty do dnia dzisiejszego do godziny 12:00.

Metoda zwraca obiekt typu String w formacie: `"Od {data podana} do {dzisiejsza data do 12:00} minęło {a} milisekund, czyli {b} sekund, czyli {c} minut, czyli {d} godzin, czyli {e} dni."`.

Przykładowo: `"Od 2019-03-23T08:00 do 2020-03-23T12:00 minęło 31636800000 milisekund, czyli 31636800 sekund, czyli 527280 minut, czyli 8788 godzin, czyli 366 dni."`