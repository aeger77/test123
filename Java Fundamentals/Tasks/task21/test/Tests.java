import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;

public class Tests {
  @Test
  public void testSolution() {
    LocalDateTime input = LocalDateTime.of(LocalDate.now(), LocalTime.of(11, 0, 30));
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

    String result = Task.calculateDifference(input.format(dateTimeFormatter));

    assertEquals("Od 2020-03-23T11:00:30 do 2020-03-23T12:00 minęło 3570000 milisekund, czyli 3570 sekund, czyli 59 minut, czyli 0 godzin, czyli 0 dni.", result);
  }

  @Test
  public void testSolution2() {
    LocalDateTime input = LocalDateTime.of(LocalDate.of(1913, 11, 17), LocalTime.of(11, 0, 30));
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

    String result = Task.calculateDifference(input.format(dateTimeFormatter));

    assertEquals("Od 1913-11-17T11:00:30 do 2020-03-23T12:00 minęło 3356038770000 milisekund, czyli 3356038770 sekund, czyli 55933979 minut, czyli 932232 godzin, czyli 38843 dni.", result);
  }
}