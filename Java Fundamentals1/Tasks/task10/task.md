Napisz program, który pobiera od użytkownika liczbę całkowitą i wypisuje w konsoli wszystkie jej dzielniki. 

Przykładowo dla liczby 21, program powinien wypisać w konsoli liczby: 1, 3, 7, 21
 
Dane pobierz od użytkownika w konsoli za pomocą klasy `Scanner`. 