package edu.grcy.prog1;

import java.util.Scanner;

public class MinutesToYears {


    public static final int MINUTES_IN_YEAR = (365 * 24 * 60);
    public static final int MINUTES_IN_DAY = (24 * 60);
    public static final int HOURS_IN_DAY = 60;

    public  static void minutesInYears(long minutes){

        long years, days, hours, leftMinutes;

        if (minutes <0L){

            System.out.println("Invalid entry value: " + minutes);
            return;
        }

        years = minutes / MINUTES_IN_YEAR;
        if (years <0L) years = 0L;
        leftMinutes = minutes - years * MINUTES_IN_YEAR;

        days = leftMinutes / MINUTES_IN_DAY;

        if (days <0L) days = 0L;

        leftMinutes -= days * MINUTES_IN_DAY;

        hours = leftMinutes / HOURS_IN_DAY;

        if (hours < 0L) hours = 0L;
        leftMinutes -= hours * HOURS_IN_DAY;

        System.out.println(minutes + "min = " + years + " years " + days + " days " + hours + " Hours " + leftMinutes + " minutes ");

    }
    public static void main(String[] args) {
        //Scanner
       // minutesInYears(1235678L);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe do sprawdzenia ");
         long liczba = scanner.nextLong();
        System.out.println("Zamiana minut na lata, dni oraz minuty");
        minutesInYears(liczba);
    }
}
