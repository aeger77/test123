package pl.sdacademy.basic;

import pl.sdacademy.basic.data.Citizen;
import pl.sdacademy.basic.data.Country;
import pl.sdacademy.basic.data.Native;
import pl.sdacademy.basic.logic.NativeAssembler;
import pl.sdacademy.basic.logic.NativeAssembler2;

import static pl.sdacademy.basic.logic.NativeAssembler2.convert;


public class Application {

    private static final String MESSAGE_1 = "Jestem ";

    public static void main(String[] args) {
        Citizen polishCitizen = new Citizen("Ania", "Nowa", 12938102938L);
        //		polishCitizen.setLastName("Książek");
        Country country = new Country();
        country.setName("Polska");
        polishCitizen.setCountry(country);

        country.setName("republika banansowa");

        Citizen secondCitizen = new Citizen("Jan", "Kowalski", 99999);
        Country countryUK = new Country();
        countryUK.setName("UK");
        secondCitizen.setCountry(countryUK);

        System.out.println(MESSAGE_1 + polishCitizen.getLastName());
        if (polishCitizen.getCountry() != null) {
            System.out.println("Mieszkam w " + polishCitizen.getCountry().getName());
        }
        System.out.println("Mój pesel " + polishCitizen.getPersonalId());

        Native nativeCitizen;
        NativeAssembler nativeAssembler = new NativeAssembler();
        nativeCitizen = nativeAssembler.convert(polishCitizen);
        System.out.println("Native ma jezyk: " + nativeCitizen.getLanguage());
        nativeCitizen = convert(polishCitizen);
        System.out.println("Native (static) ma jezyk: " + nativeCitizen.getLanguage());

    }
}

