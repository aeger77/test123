package pl.sdacademy.basic.example;

public class Album {

    private String name;

    //FIXME ; inaczej narazie nie umiem robiv
    private Photo[] photos = new Photo[1000];

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;


    }

    public void addPhoto(Photo photo, int index) {

        this.photos[index]=photo;
    }

    public  void show(){
        for (Photo photo: photos) {
            if (photo !=null)
            {
                System.out.println("Zdjecie  " + photo.getName());
            }
        }
    }
    public static void main(String[] args) {

        Album architecture = new Album();

        architecture.setName("Architekura");

        Album people = new Album();
        people.setName("Ludzie");

        Album rome = new Album();
        rome.setName("Rzym");

        Album vennice = new Album();
        vennice.setName("Wenecja");

        Photo[] photosAll = new Photo[1000];
        for (int i = 0; i < 1000 ; i++) {

            photosAll[i]= new Photo("name" + i, "place" + i);

        }
        architecture.addPhoto(photosAll[5], 0);
        rome.addPhoto(photosAll[5],1);

        people.addPhoto(photosAll[20], 0);

        architecture.addPhoto(photosAll[120], 1);
        vennice.addPhoto(photosAll[120], 1);

        architecture.show();

    }
}
