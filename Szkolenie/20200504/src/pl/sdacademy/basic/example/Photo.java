package pl.sdacademy.basic.example;

public class Photo {

    private String name;
    private String place;

    public Photo(String name, String place){
        this.name = name;
        this.place = place;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
}
