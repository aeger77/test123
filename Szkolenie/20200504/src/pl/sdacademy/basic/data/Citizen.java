package pl.sdacademy.basic.data;

public class Citizen {

    private String firstName;
    private String lastName;
    private long personalId;

    private Country country;

    public Citizen() {
    }

    public Citizen(String firstName, String lastName, long personalId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.personalId = personalId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public long getPersonalId() {
        return personalId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPersonalId(long personalId) {
        this.personalId = personalId;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }


}
