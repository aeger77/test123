package pl.sdacademy.basic;

public class Person {
    private String firstName;
    private String secondName;
    private String birthCity;

    public Person (String firstName, String secondName, String birthCity ){

        this.firstName=firstName;
        this.secondName=secondName;

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getBirthCity() {
        return birthCity;
    }

    public void setBirthCity(String birthCity) {
        this.birthCity = birthCity;
    }

    public static void main(String[] args) {

        //Person = new Person()
    }
}
