package pl.sdacademy.basic.logic;

import pl.sdacademy.basic.data.Citizen;
import pl.sdacademy.basic.data.Native;

public class NativeAssembler2 {

    public static Native convert(Citizen citizen) {
        Native nativeCitizen = new Native();
        nativeCitizen.setFirstName(citizen.getFirstName());
        nativeCitizen.setLastName(citizen.getLastName());
        if (citizen.getCountry() != null) {
            nativeCitizen.setLanguage(citizen.getCountry().getLanguage());
        }
        return nativeCitizen;
    }

}
