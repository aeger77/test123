package pl.sdacademy.basic.logic;

import pl.sdacademy.basic.data.Citizen;
import pl.sdacademy.basic.data.Native;

public class NativeAssembler {
    public Native convert(Citizen citizen) {
        Native nativeCitizen = new Native();
        nativeCitizen.setFirstName(citizen.getFirstName());
        nativeCitizen.setLastName(citizen.getLastName());
        optionalLanguageSet(citizen, nativeCitizen);
        return nativeCitizen;
    }

    private void optionalLanguageSet(Citizen citizen, Native nativeCitizen) {
        if (citizen.getCountry() != null) {
            nativeCitizen.setLanguage(citizen.getCountry().getLanguage());
        }
    }
}
