import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        for (int i = 0; i < n ; i++) {

            int sqr = (int) Math.pow(2,i);
            if (sqr<=n) {
                System.out.println(sqr + " ");
            }else {

                System.out.println("Kończę na i = " + i);
                break;
            }
        }

    }
}
