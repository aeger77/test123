import javax.sound.midi.Soundbank;
import java.util.Scanner;

public class MinMax1 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String numbersInLine = scanner.nextLine();

        System.out.println("moje liczby " + numbersInLine);
        String[] numbers = numbersInLine.split( ",");

        //W kolejnych krokach bedziemy ktore liczby sa mniejsze. Musimy zaczac od duzej liczby
        //Aby w pierwszej kolejnosci z naszych liczb została znaleziona jako mniejsza

        int min = Integer.MIN_VALUE;
        int max = Integer.MIN_VALUE;

        int current;
        for (String number: numbers) {

            if ("0".equals(number)){
                break;
            }
            current = Integer.parseInt(number);
            if (current < min) {
                min = current;
            }
            if (current > max){
                max = current;
            }
        }

        System.out.println("Suma min + max" + (min+max));
        System.out.println("Srednia arytmetyczna " + (max + min) / 2.0f);


    }
}
