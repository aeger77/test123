import java.util.Scanner;

public class Sprawdzenie_Wagi {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj wzrost");
        int height = scanner.nextInt();
        System.out.println("Podaj wagę");
        float weight = scanner.nextFloat();

        if (height <= 150 || weight > 180.0f) {
            System.out.println("Przykro mi, nie możesz jechać!");
        } else {
            System.out.println("Zapnij pasy!");
        }
    }
}
