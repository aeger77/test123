package pl.eddie.data;

public class Address {

    private String city;
    private String street;
    private boolean registeredAddress = false;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public boolean getRegisteredAddress() {
        return registeredAddress;
    }

    public void setRegisteredAddress(boolean registeredAddress) {
        this.registeredAddress = registeredAddress;
    }
}
