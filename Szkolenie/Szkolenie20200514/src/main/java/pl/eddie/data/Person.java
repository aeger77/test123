package pl.eddie.data;
import java.time.LocalDate;
import java.time.Period;

public class Person {

    private String firstName;
    private String lastName;
    // birthPlace byłoby zbyt ogólną nazwą
    private String birthCity;
    private Address address = new Address();

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthCity() {
        return birthCity;
    }

    public void setBirthCity(String birthCity) {
        this.birthCity = birthCity;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    void sayHello() {
        // jesteśmy wewnątrz obiektu danej klasy dlatego odwołujemy się tylko i wyłącznie do zmiennych
        // użycie address.getCity() nie ma tego problemu (code smell) co poprzednie rozwiązanie!
        System.out.println("Witam, jestem " + firstName + " " + lastName + ", mieszkam w " + address.getCity());
    }


    public static void main(String[] args) {
        Address address = new Address();
        address.setCity("Bytom");
        address.setStreet("Dworcowa");


        Person me = new Person();
        me.setFirstName("Anna");
        me.setLastName("Książek");
        me.setBirthCity("Bytom");
        me.setAddress(address);

        Person brother = new Person();
        brother.setFirstName("Jan");
        brother.setLastName("Nowak");
        brother.setBirthCity("Bytom");
        brother.setAddress(address);

        me.sayHello();
        brother.sayHello();

        address.setCity("error");

        // ponieważ Person operuje na obiekcie poprzez zmienną, zmiana wartości w adresie jest od razu widoczna w obiektach używających tej zmiennej.
        // FIXME: jest to cecha, która może być niebezpieczna. Czasem jest celowo wykorzystywana. Rozwiązaniem tego problemu jest operowanie na klasach immutable.
        // Immutable jest jednym z ważniejszych elementów Javy. Kolejne bloki uzupełnią wiedzę w tym zakresie!
        me.sayHello();
        brother.sayHello();
    }


}
