package pl.sdacademy;

import static pl.sdacademy.tools.Calculator2.divide;
import static pl.sdacademy.tools.Calculator2.mulitply;
import static pl.sdacademy.tools.Calculator2.subtract;

import java.util.Scanner;

import pl.sdacademy.tools.Calculator2;

public class Calculator2Test {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);


		System.out.print("Podaj operację (+ lub - lub * lub /):");
		String operation = scanner.next();

		System.out.print("Argumenty (rozdziel ','):");
		String arguments = scanner.next();

		String[] opArgs = arguments.split(",");

		if (opArgs.length < 2 || "/".equals(operation) && opArgs.length > 2) {
			System.out.println("Nieprawidłowe parametry - kończę!");
			return;
		}


		String title = "Wynik: ";
		switch (operation.trim()) {
		case "+":
			// w tym miejscu jest odwołanie wprost do metody statycznej
			// w kolejnych przypadkach użyty jest dostęp uproszczony wykorzystujący import static (patrz sekcję importów na samej górze)
			System.out.println(title + Calculator2.add(Integer.valueOf(opArgs[0]), Integer.valueOf(opArgs[1]), normalizeExtraArgs(opArgs)));
			return;
		case "-":
			System.out.println(title + subtract(Integer.valueOf(opArgs[0]), Integer.valueOf(opArgs[1]), normalizeExtraArgs(opArgs)));
			return;
		case "/":
			System.out.println(title + divide(Integer.valueOf(opArgs[0]), Integer.valueOf(opArgs[1])));
			return;
		case "*":
			System.out.println(title + mulitply(Integer.valueOf(opArgs[0]), Integer.valueOf(opArgs[1]), normalizeExtraArgs(opArgs)));
			return;
		default:
			System.out.println("Niepoprawna operacja!");
		}
	}

	private static int[] normalizeExtraArgs(String[] opArgs) {
		if (opArgs.length == 2) {
			return new int[0];
		}
		int[] result = new int[opArgs.length - 2];
		for (int i = 2; i < opArgs.length; i++) {
			result[i - 2] = Integer.valueOf(opArgs[i]);
		}
		return result;
	}
}
