package pl.sdacademy;

import java.util.Scanner;

import pl.sdacademy.tools.Calculator;

public class CalculatorTest {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		// warto podawać oczekiwane wartości - program jest wtedy user friendly
		// zauważ, że użyte jest print a nie println - kursor zostaje za opisem
		System.out.print("Podaj operację (+ lub - lub * lub /):");
		String operation = scanner.next();

		System.out.print("Argumenty (rozdziel ','):");
		String arguments = scanner.next();

		String[] opArgs = arguments.split(",");
		// brak kontroli elementów dostarczanych (parametrów wejściowych) jest jednym z głównych powodów awarii.
		// kontrola jest dobrą praktyką
		if (opArgs.length < 2 || "/".equals(operation) && opArgs.length > 2) {
			System.out.println("Nieprawidłowe parametry - kończę!");
			return;
		}

		Calculator calculator = new Calculator();
		// poniższe rozwiązanie nie jest doskonałe, ponieważ nie kontroluje czy parametry na pewno są liczbami
		// wykorzystany jest wewnętrzny mechanizm javy, który obsłuży taką sytuację.
		// w kolejnych blokach zostanie przedstawione usprawnienie w tym zakresie
		String title = "Wynik: ";
		switch (operation.trim()) {
		case "+":
			System.out.println(title + calculator.add(Integer.valueOf(opArgs[0]), Integer.valueOf(opArgs[1]), normalizeExtraArgs(opArgs)));
			return;
		case "-":
			System.out.println(title + calculator.subtract(Integer.valueOf(opArgs[0]), Integer.valueOf(opArgs[1]), normalizeExtraArgs(opArgs)));
			return;
		case "/":
			System.out.println(title + calculator.divide(Integer.valueOf(opArgs[0]), Integer.valueOf(opArgs[1])));
			return;
		case "*":
			System.out.println(title + calculator.mulitply(Integer.valueOf(opArgs[0]), Integer.valueOf(opArgs[1]), normalizeExtraArgs(opArgs)));
			return;
		default:
			System.out.println("Niepoprawna operacja!");
		}
	}

	// powtarzalne elementy są ujmowane w wydzilonych metodach, zwykle prywatnych
	private static int[] normalizeExtraArgs(String[] opArgs) {
		if (opArgs.length == 2) {
			// poniższy wynik to tablica pusta (z wymiarem 0), która nie będzie zmianać w żaden sposób wyniku
			return new int[0];
		}
		int[] result = new int[opArgs.length - 2];
		for (int i = 2; i < opArgs.length; i++) {
			// do nowej tablicy musimy zapisywać od indeksu = 0, wejściową tabelę przeglądamy od parametry 3 (o indeksie 2).
			// [i-2] pozwala nam zsynchronizować te dwie operacje bez konieczności prowadzania dodakowej zmiennej
			result[i - 2] = Integer.valueOf(opArgs[i]);
		}
		return result;
	}
}
