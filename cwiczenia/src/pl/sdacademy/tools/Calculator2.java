package pl.sdacademy.tools;

public class Calculator2 {

	public static int add(int first, int second, int... others) {
		int result = first + second;
		for (int other : others) {
			result += other;
		}
		return result;
	}

	public static int mulitply(int first, int second, int... others) {
		int result = first * second;
		for (int other : others) {
			result *= other;
		}
		return result;
	}

	public static int subtract(int first, int second, int... others) {
		int result = first - second;
		for (int other : others) {
			result -= other;
		}
		return result;
	}

	public static double divide(int first, int second) {
		return first / second;
	}
}
