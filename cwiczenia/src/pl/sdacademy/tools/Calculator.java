package pl.sdacademy.tools;

public class Calculator {

	public int add(int first, int second, int... others) {
		int result = first + second;
		for (int other : others) {
			result += other;
		}
		return result;
	}

	public int mulitply(int first, int second, int... others) {
		int result = first * second;
		for (int other : others) {
			result *= other;
		}
		return result;
	}

	public int subtract(int first, int second, int... others) {
		int result = first - second;
		for (int other : others) {
			result -= other;
		}
		return result;
	}

	public double divide(int first, int second) {
		return first / second;
	}
}
