public class Task01 {
	public static void main(String[] arg) {
		int bigger = 12;
		int smaller = 5;
		int result = bigger - smaller;

		String lastMessage = "";
		for (int i = 1; i <= bigger; i++) {
			if (i == bigger) {
				System.out.println("Pierwsza liczba "+ bigger);
				continue;
			}
			if (i == smaller) {
				System.out.println("Druga liczba " + smaller);
				continue;
			}
			if (i == result) {
				lastMessage = "Wynik odejmowania " + i;
			}
		}

		System.out.println(lastMessage);
	}
}
